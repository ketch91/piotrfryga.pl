<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Piotr Fryga - Front-End Developer, Poznań</title>
<meta name="description" content="piotrfryga.pl - Front-End Developer, Poznań">
<meta name="keywords" content="piotr fryga, krotoszyn, poznan, websites, projektowanie stron internetowych, strony www, ikrotoszyn, ijarocin, front-end developer, webdeveloper">
<link href="css/style.min.css" rel="stylesheet" media="screen">
<link href="css/mobile.css" rel="stylesheet" media="screen">
</head>
<body data-responsejs='{ 
    "create": [{ 
        "prop": "width"
      , "prefix": "min-width- r src"
      , "breakpoints": [800, 961, 1025, 1281, 1367, 1601] 
  }]
}'>
<div id="top"></div>
<div id="pfTopSlide" class="default_menu">
	<div class="pfCenterBlock">
		<div id="pfTopSlideLogo">
			<span><a class="pfLogoLink" href="#top" ><img id="pfLogo" src="img/logo.png" alt="logo" /></a><a class="pfLogoLink" href="#top" ><h2 id="pfLogoh2">Front-End Developer</h2></a></span>
		</div>
		<div class="nav">
			<ul>
				<li class="active"><a href="#top">Home</a></li>
				<li><a href="#pfWork">Projects</a></li>
				<li><a href="#pfProfile">About</a></li>
				<li><a href="#pfContact">Contact</a></li>
			</ul>
		</div>
	</div>
</div>
<div id="pfHome">
	<div class="pfHomeLoader"></div>
	<img id="pfPoznanImage" src="#" data-src800="img/poznan/poznan800.jpg" data-src961="img/poznan/poznan1024.jpg" data-src1025="img/poznan/poznan1281.jpg" data-src1281="img/poznan/poznan1367.jpg" data-src1367="img/poznan/poznan1600.jpg" data-src1601="img/poznan/poznan1920.jpg" alt="poznan" />
	<div class="hello">
		<h1>
			<span class="first_line">Selected projects of<br>Piotr Fryga<br></span>
			<span class="second_line">Web Development - Poznań, Poland.</span>
		</h1>
	</div>
</div>
<div id="pfContent">
	<div id="pfWorkItemPresentation">
		<div class="pfCenterBlock">
			<div class="pfClose"></div>
			<h3 id="pfProjectTitle">Breip.com</h3>
			<div id="pfProjectFramePhoto" class="pfProjectFramePhoto">
				<img id="pfProjectBigPhoto" src="#" alt="big" />
				<div id="pfProjectDescBlock"><span id="pfProjectDesc">Project is avaliable at xxx</span></div>
			</div>
		</div>
	</div>
	<div class="pfCenterBlock">
		<div id="pfWork">
			<h3>Projects</h3>
			<div id="pfWork_container" class="pfWork_container">
				<div class="pfWork_nav">
					<span id="tj_prev" class="tj_prev">Previous</span>
					<span id="tj_next" class="tj_next">Next</span>
				</div>
				<div class="pfWork_wrapper">
					<ul class="tj_gallery">
						<!-- motosanitor.pl -->
						<!-- <li>
							<span class="displayHoverConent">
								<div class="pfWorkItemHover">
									<h3>motosanitor.pl website </h3>
									<div class="pfWorkItemHoverZoom"></div>
									<h5>Avaliable on-line at <a class="projectItemExternalUrl" href="http://www.motosanitor.pl" target="_blank" alt="motosanitor">motosanitor.pl</a></h5>
								</div>
								<img class="pfProjectScreen" src="img/smallProjects/motosanitor.jpg" alt="motosanitor" data-large="img/bigProjects/motosanitor2.jpg" />
							</span>
						</li> -->
						<!-- drapaka.pl -->
						<li>
							<span class="displayHoverConent">
								<div class="pfWorkItemHover">
									<h3>Drapaka.pl website </h3>
									<div class="pfWorkItemHoverZoom"></div>
									<h5>Avaliable on-line at <a class="projectItemExternalUrl" href="http://www.drapaka.pl" target="_blank" alt="drapaka">drapaka.pl</a></h5>
								</div>
								<img class="pfProjectScreen" src="img/smallProjects/drapaka.jpg" alt="drapaka" data-large="img/bigProjects/drapaka2.jpg" />
							</span>
						</li>
						<!-- Breip.com -->
						<li>
							<span class="displayHoverConent">
								<div class="pfWorkItemHover">
									<h3>Breip. website </h3>
									<div class="pfWorkItemHoverZoom"></div>
									<h5>Avaliable on-line at <a class="projectItemExternalUrl" href="http://www.breip.com" target="_blank" alt="breip">breip.com</a></h5>
								</div>
								<img class="pfProjectScreen" src="img/smallProjects/breip.jpg" alt="breip" data-large="img/bigProjects/breip.jpg" />
							</span>
						</li>
						<!-- Breip.com admin -->
						<li>
							<span class="displayHoverConent">
								<div class="pfWorkItemHover">
									<h3>Breip.com website admin</h3>
									<div class="pfWorkItemHoverZoom"></div>
									<h5>Avaliable only for breip.com administrators</h5>
								</div>
								<img class="pfProjectScreen" src="img/smallProjects/breip_zaplecze.jpg" alt="breip admin"  data-large="img/bigProjects/breip_zaplecze.jpg" />
							</span>
						</li>
						<!-- opony - myjnie -->
						<li>
							<span class="displayHoverConent">
								<div class="pfWorkItemHover">
									<h3>Website of car wash</h3>
									<div class="pfWorkItemHoverZoom"></div>
									<h5>Avaliable on-line at <a class="projectItemExternalUrl" href="http://www.opony-myjnie.pl" target="_blank" alt="opony, myjnie">opony-myjnie.pl</a></h5>
								</div>
								<img class="pfProjectScreen" src="img/smallProjects/opony.jpg" alt="Opony, myjnie" data-large="img/bigProjects/opony.jpg"/>
							</span>
						</li>
						<!-- studiomebli -->
						<li>
							<span class="displayHoverConent">
								<div class="pfWorkItemHover">
									<h3>Website of furniture showroom "Hanna"</h3>
									<div class="pfWorkItemHoverZoom"></div>
									<h5>Avaliable on-line at <a class="projectItemExternalUrl" href="http://www.studiomebli.org" target="_blank" alt="studiomebli, myjnie">studiomebli.org</a></h5>
								</div>
								<img class="pfProjectScreen" src="img/smallProjects/studiomebli.jpg" alt="studiomebli" data-large="img/bigProjects/studiomebli.jpg"/>
							</span>
						</li>
						<!-- pcpr -->
						<li>
							<span class="displayHoverConent">
								<div class="pfWorkItemHover">
									<h3>Website of Poviat Family Support Centre in Krotoszyn</h3>
									<div class="pfWorkItemHoverZoom"></div>
									<h5>Avaliable on-line at <a class="projectItemExternalUrl" href="http://www.pcpr.krotoszyn.pl" target="_blank" alt="pcpr">pcpr.krotoszyn.pl</a></h5>
								</div>
								<img class="pfProjectScreen" src="img/smallProjects/pcpr.jpg" alt="pcpr" data-large="img/bigProjects/pcpr.jpg" />
							</span>
						</li>
						<!-- wdd -->
						<li>
							<span class="displayHoverConent">
								<div class="pfWorkItemHover">
									<h3>Website of "Wszystko dla domu" shop</h3>
									<div class="pfWorkItemHoverZoom"></div>
									<h5>Avaliable on-line at <a class="projectItemExternalUrl" href="http://www.wdd.ikrotoszyn.com" target="_blank" alt="wszystko dla domu">wdd.ikrotoszyn.com</a></h5>
								</div>
								<img class="pfProjectScreen" src="img/smallProjects/wdd.jpg" alt="wdd" data-large="img/bigProjects/wdd.jpg"/>
							</span>
						</li>
						<!-- kamienskie -->
						<li>
							<span class="displayHoverConent">
								<div class="pfWorkItemHover">
									<h3>Kamienskie.info news portal</h3>
									<div class="pfWorkItemHoverZoom"></div>
									<h5>Avaliable on-line at <a class="projectItemExternalUrl" href="http://www.kamienskie.info" target="_blank" alt="kamienskie, myjnie">kamienskie.info</a></h5>
								</div>
								<img class="pfProjectScreen" src="img/smallProjects/kamienskie.jpg" alt="kamienskie" data-large="img/bigProjects/kamienskie.jpg"/>
							</span>
						</li>
						<!-- fabula -->
						<li>
							<span class="displayHoverConent">
								<div class="pfWorkItemHover">
									<h3>Website of Fabuła club</h3>
									<div class="pfWorkItemHoverZoom"></div>
									<h5>Avaliable on-line at <a class="projectItemExternalUrl" href="http://www.fabula.ikrotoszyn.com" target="_blank" alt="fabula">fabula.ikrotoszyn.com</a></h5>
								</div>
								<img class="pfProjectScreen" src="img/smallProjects/fabula.jpg" alt="fabula" data-large="img/bigProjects/fabula.jpg" />
							</span>
						</li>
						<!-- sio -->
						<li>
							<span class="displayHoverConent">
								<div class="pfWorkItemHover">
									<h3>Website of "SIO" association</h3>
									<div class="pfWorkItemHoverZoom"></div>
									<h5>Avaliable on-line at <a class="projectItemExternalUrl" href="http://www.sio-krotoszyn.pl" target="_blank" alt="sio">sio-krotoszyn.pl</a></h5>
								</div>
								<img class="pfProjectScreen" src="img/smallProjects/sio.jpg" alt="sio" data-large="img/bigProjects/sio.jpg" />
							</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div id="pfProfile">
			<h3>About</h3>
			<div class="pfProfileSummary">
				<h4>Summary</h4>
				<p>
					My name is Piotr and I'm web designer and web developer. I live in <span aria-hidden="true" class="icon-location"></span> Poznań, Poland. At that moment I'm studying Computer Science at Poznań Univeristy of Technology. Since I remember I've always had a major passion for web applications. I’ve been still trying to improve my skills. You can find me on social websites. There are some below. If you need more information, contact me.
				</p>
			</div>
			<div class="pfProfileSkills">
				<h4>Skills</h4>
				<ul class="pfProfileSkillsList">
					<li>
						<span>
							<div class="pfProgileSkillsStars">
								<div class="pfProfileSkillsStarItem null" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
							</div>
							HTML5
						</span>
					</li>
					<li>
						<span>
							<div class="pfProgileSkillsStars">
								<div class="pfProfileSkillsStarItem null" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
							</div>
							CSS3 (+ less.js preprocessor)
						</span>
					</li>
					<li>
						<span>
							<div class="pfProgileSkillsStars">
								<div class="pfProfileSkillsStarItem null" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
							</div>
							Javascript (+ jQuery)
						</span>
					</li>
					<li>
						<span>
							<div class="pfProgileSkillsStars">
								<div class="pfProfileSkillsStarItem null" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
							</div>
							Photoshop
						</span>
					</li>
					<li>
						<span>
							<div class="pfProgileSkillsStars">
								<div class="pfProfileSkillsStarItem null" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem null" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
							</div>
							PHP (with Symfony2) + SQL
						</span>
					</li>
					<li>
						<span>
							<div class="pfProgileSkillsStars">
								<div class="pfProfileSkillsStarItem null" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem null" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
								<div class="pfProfileSkillsStarItem full" aria-hidden="true" data-icon="&#xe017;"></div>
							</div>
							Git
						</span>
					</li>
				</ul>
			</div>
			<div class="pfProfileExperience">
				<h4>Work experience</h4>
				<ul id="pfProfileExperienceList">
					<li>
						<div class="pfImage">
							<img src="img/logo_allegro.png" alt="allegro" />
						</div>
						<div class="pfProfileListContent">
							<h5>Junior Front-End Developer</h5>
							<span class="company">Grupa Allegro Sp. z o.o.</span>
							<span class="jobTime">October 2012 - present</span>
							<p>
								As a team we're making user interface using HTML5, CSS3 and JavaScript (including many libraries) accordance with RWD technology. We are working with several utility tools like SublimeText2 and Photoshop CS6.
							</p>
						</div>
					</li>
					<li>
						<div class="pfImage">
							<img src="img/logo_breip.png" alt="breip" />
						</div>
						<div class="pfProfileListContent">
							<h5>Web Developer</h5>
							<span class="company">Breip.</span>
							<span class="jobTime">July 2012 - December 2012</span>
							<p>
								It's a specialist portal for everyone who is interested in civil engineering objects building and designing. We've used PHP5, MySQL, HTML5, CSS3, JavaScript with jQuery framework. Project is still being developed. The website is avaliable on-line at <a href="http://www.breip.com/" target="_blank">breip.com</a>
							</p>
						</div>
					</li>
					<li>
						<div class="pfImage">
							<img src="img/verax_logo_transp.png" alt="verax" />
						</div>
						<div class="pfProfileListContent">
							<h5>Web Developer</h5>
							<span class="company">Verax Systems Corp.</span>
							<span class="jobTime">July 2012 - September 2012</span>
							<p>
								We've developed company website - <a href="http://www.veraxsystems.com/" target="_blank">www.veraxsystems.com</a>. We designed new look of homepage and products pages.
							</p>
						</div>
					</li>
					<li>
						<div class="pfImage">
							<img src="img/logo_freelance.png" alt="freelance" />
						</div>
						<div class="pfProfileListContent">
							<h5 class="freelancerH5">Freelancer</h5>
							<p>
								Since 2007 I have been working as freelancer. I created many small websites using Wordpress, Drupal or Joomla! CMS.
							</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div id="pfContact">
		<h3>Contact</h3>

		<div class="pfContactDataBlock">
			<span id="pfContactGetInTouch">If you are interested in cooperatoin, there is contact below.</span>
			<div class="pfContactItemBlock">
				<div id="pfContactEmailAddressBlock" class="pfIcons" aria-hidden="true" data-icon="&#xe00f;"><span id="pfEmailAddress"> piotr.fryga@gmail.com</span></div>
			</div>
			<div class="pfContactItemBlock">
				<div id="pfContactMobileBlock" class="pfIcons" aria-hidden="true" data-icon="&#xe013;"><span id="pfMobile"> +48 514 665 783</span></div>
			</div>

			<h4 id="pfContactGetSocialH4">Get social</h4>
			<ul class="pfSocialList">
				<li><a href="http://www.linkedin.com/pub/piotr-fryga/5a/675/970" target="_blank"><div class="pfSocialIcons" aria-hidden="true" data-icon="&#xe00c;"></div></a></li>
				<li><a href="https://www.facebook.com/piotr.fryga" target="_blank"><div class="pfSocialIcons" aria-hidden="true" data-icon="&#xe009;"></div></a></li>
				<li><a href="https://plus.google.com/u/0/106281741985502593379/posts/p/pub" target="_blank"><div class="pfSocialIcons" aria-hidden="true" data-icon="&#xe008;"></div></a></li>
				<li><a href="https://twitter.com/ketch91" target="_blank"><div class="pfSocialIcons" aria-hidden="true" data-icon="&#xe00a;"></div></a></li>
				<li><a href="http://www.lastfm.pl/user/ketch91" target="_blank"><div class="pfSocialIcons" aria-hidden="true" data-icon="&#xe00d;"></div></a></li>
			</ul>

		</div>
		<div class="pfContactFormBlock">
			<h4 id="pfContactFormH4">Quick e-mail form</h4>
			<div class="pfContactFormPaper">
				<form action="mail.php" method="POST" id="pfContactForm">
				    <fieldset id="inputs">
				        <input id="name" name="name" type="text" placeholder="Your name" required>   
				        <input id="email" name="email" type="email" placeholder="Your e-mail" required>
				        <textarea id="form_content" name="form_content" cols="30" rows="5" placeholder="Your message" required></textarea>
				    </fieldset>
				    <fieldset id="actions">
				        <input type="submit" id="submit" value="Send it!">
				    </fieldset>
				</form>
			</div>
		</div>

		</div>
	</div>
	<div id="pfBottom">
		<div class="pfBottomContainer">
			<p class="copyright">© 2013, Piotr Fryga. All rights reserved.</p>
			<div class="pfBottomBackToTop">
				<a href="#top" class="pfBottomBackToTopLink"></a>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.gridnav.min.js"></script>
<script type="text/javascript" src="js/responsive.min.js"></script>
<script src="js/ketch.min.js" type="text/javascript"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39204976-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>