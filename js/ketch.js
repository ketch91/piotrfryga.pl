/* ===================================================
 * ketch.js
 * http://ikrotoszyn.com
 * February 2013
 * ========================================================== */

 var  bigImage = new Image(),
      lastId,
      topMenu = $('#pfTopSlide'),
      topMenuHeight = topMenu.outerHeight(),
      menuItems = topMenu.find("a"),
      scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if(item.length) {return item;}
      });

/* ================== Smooth scroll ==================== */
$('a[href^="#"]').bind('click.smoothscroll',function (e) {
  e.preventDefault();
  var target = this.hash;
      $target = $(target);
  $('html, body').stop().animate({
      'scrollTop': $target.offset().top
  }, 500, 'swing', function () {
      window.location.hash = target;
  });
});

/* ================== Count top shift ================== */
$(document).ready(function(){
  $('#pfContent').css('top', $("#pfHome").height());

  $(window).resize(function(){
    $('#pfContent').css('top', $("#pfHome").height());
  });
});

/* ============== Scroll window - fix menu ============= */

$(function(){

  var menu = $('#pfTopSlide'),
    pos = menu.offset(),
    target = $('#pfPoznanImage'),
    targetHeight = $("#pfHome").height();

    $(window).scroll(function(){
      /* count main photo opacity */
      var scrollPercent = (targetHeight - window.scrollY) / targetHeight;
      if(scrollPercent >= 0){
        target.css('opacity', scrollPercent);
      }
      if($(this).scrollTop() > pos.top+menu.height() && menu.hasClass('default_menu')){
        menu.fadeOut('normal', function(){
          $(this).removeClass('default_menu').addClass('fixed_menu').fadeIn('normal');
        });
      } else if($(this).scrollTop() <= pos.top && menu.hasClass('fixed_menu')){
        menu.fadeOut('normal', function(){
          $(this).removeClass('fixed_menu').addClass('default_menu').fadeIn('normal');
        });
      }
      /* end of count main photo opacity */

      /* top menu current item */
      var fromTop = $(this).scrollTop()+topMenuHeight;
      if(fromTop > 2100) {
        var id = "pfContact";
      } else {
        var cur = scrollItems.map(function(){
          if ($(this).offset().top < fromTop)
          return this;
        });

        cur = cur[cur.length-1];
        var id = cur && cur.length ? cur[0].id : "";
      }

      if (lastId !== id) {
        lastId = id;
        menuItems
        .parent().removeClass("active")
        .end().filter("[href=#"+id+"]").parent().addClass("active");
      }
      /* end of top menu current item */
    });

});

/* ========= Projects list - vertical carousel ========= */
/* ============ Show contact items on load ========== */

$(function() {
  $('#pfWork_container').gridnav({
    type  : {
      mode    : 'sequpdown',  // use def | fade | seqfade | updown | sequpdown | showhide | disperse | rows
      speed   : 200,      // for fade, seqfade, updown, sequpdown, showhide, disperse, rows
      easing    : '',     // for fade, seqfade, updown, sequpdown, showhide, disperse, rows 
      factor    : 50,     // for seqfade, sequpdown, rows
      reverse   : false     // for sequpdown
    }
  });
  $('#pfContactEmailAddressBlock, #pfContactMobileBlock').show();
  $('#submit').show();
});

/* =========== Projects list - hover on item =========== */

$(".displayHoverConent").hover(function() {
    $(this).children(".pfWorkItemHover").fadeIn();
}, function() {
     $(this).children(".pfWorkItemHover").fadeOut();
});

/* =========== Click on project item =================== */

// show bigger photo from external URL
$(".displayHoverConent").click(function() {
  $("#pfProjectBigPhoto").attr('src','');
  $('#pfProjectFramePhoto').addClass('pfLoadingImage').children('img').hide();
  $('#pfProjectTitle').empty().append($(this).children('.pfWorkItemHover').children('h3').html());
  $('#pfProjectDesc').empty().append($(this).children('.pfWorkItemHover').children('h5').html());
  $("#pfWorkItemPresentation").show("slide",{direction: 'left'})
  
  bigImage.src = $(this).children("img").data("large");
  $('#pfProjectBigPhoto').attr('src', bigImage.src);
  $('#pfProjectBigPhoto').load(function () {
     $('#pfProjectFramePhoto').removeClass('pfLoadingImage').children('img').fadeIn();
  });
});

// hide bigger photo from external URL
$("#pfContent .pfCenterBlock .pfClose").click(function() {
  $("#pfWorkItemPresentation").hide("slide",{direction: 'right'});
});

/* =========== StopPropagation on external URL ========= */

$(".projectItemExternalUrl").click(function(event){
    event.stopPropagation();
});

/* ========== Disable loader on load main image ======== */

$('#pfPoznanImage').load(function () {
   $("#pfHome .pfHomeLoader").addClass('disabled');
   $(this).fadeIn();
});

/* =========== Change icon color on hover ========= */

$(".pfSocialIcons").mouseover(function() {
     $(this).stop(true, false).animate({ color:'#DD0E2A'},300);
}).mouseout(function() {
    $(this).stop(true, false).animate({ color:'#bbb'},300);
});            

/* =========== Change submit button on hover ========= */

$("#actions #submit").mouseover(function() {
     $(this).stop(true, false).animate({ 'opacity' : 0.7},300);
}).mouseout(function() {
    $(this).stop(true, false).animate({ 'opacity' : 1},300);
}); 

/* =========== Change ExperienceList items on hover ========= */

$("#pfProfileExperienceList li").mouseover(function() {
    $(this).children('.pfImage').stop(true, false).animate({ 'opacity' : 1},300);
}).mouseout(function() {
    $(this).children('.pfImage').stop(true, false).animate({ 'opacity' : 0.3},300);
}); 

/* =========== Change Logo opacity on hover ========= */

$("#pfLogo, #pfLogoh2").mouseover(function() {
    $("#pfLogo, #pfLogoh2").stop(true, false).animate({ 'opacity' : 0.7},300);
}).mouseout(function() {
    $("#pfLogo, #pfLogoh2").stop(true, false).animate({ 'opacity' : 1},300);
}); 


